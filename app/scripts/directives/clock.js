'use strict';

/**
 * @ngdoc directive
 * @name rozkladyApp.directive:clock
 * @description
 * # clock
 */
angular.module('rozkladyApp')
    .directive('clock', function ($timeout) {
        return {
            restrict: 'E',
            transclude: true,
            link: function (scope, element, attrs) {
                function addZero(i) {
                    if (i < 10) {
                        i = "0" + i;
                    }
                    return i;
                }

                var tick = function () {
                    var exact = new Date();
                    var text = addZero(exact.getHours()) + ':' + addZero(exact.getMinutes());
                    element.html(text);
                    $timeout(tick, 5000);
                };

                $timeout(tick, 1000);
            }


        };
    });
