'use strict';

/**
 * @ngdoc directive
 * @name rozkladyApp.directive:minutesTill
 * @description
 * # minutesTill
 */
angular.module('rozkladyApp')
    .directive('minutesTill', function () {
        return {
            restrict: 'E',
            transclude: true,
            replace: true,
            link: function (scope, element, attrs) {
                function addZero(i) {
                    if (i < 10) {
                        i = "0" + i;
                    }
                    return i;
                }


                var exact = new Date(attrs.time * 1000);

                var time = Math.floor((exact - new Date()) / 60000);
                var text = "";
                if (time > 60) {
                    text = addZero(exact.getHours()) + ':' + addZero(exact.getMinutes());
                } else {
                    text = time + ' m';
                }
                element.html(
                    '<div class="least-content">' + text + '</div>'
                );
            }
        };
    });
