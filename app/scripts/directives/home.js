'use strict';

/**
 * @ngdoc directive
 * @name rozkladyApp.directive:home
 * @description
 * # home
 */
angular.module('rozkladyApp')
    .directive('home', function () {
        return {
            restrict: 'E',
            replace: true,
            scope: true,
            templateUrl: "views/directives/home.html",
            controllerAs: 'home',

            controller: function (HomeFactory) {
                this.stops = [];

                this.goToStop = function (id) {

                    HomeFactory.goToStop(id);
                };

                this.getFavoriteStop = function () {
                    HomeFactory.getFavoriteStop();

                }

            },


            link: function (scope, element, attrs, ctrl) {


            }
        };
    });
