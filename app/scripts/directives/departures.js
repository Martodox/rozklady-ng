'use strict';

/**
 * @ngdoc directive
 * @name rozkladyApp.directive:departures
 * @description
 * # departures
 */
angular.module('rozkladyApp')
    .directive('departures', function () {
        return {
            restrict: 'E',
            replace: true,
            scope: true,
            templateUrl: "views/directives/departures.html",
            controllerAs: 'home',
            link: function (scope, element, attrs) {


            }
        };
    });
