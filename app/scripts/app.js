'use strict';

/**
 * @ngdoc overview
 * @name rozkladyApp
 * @description
 * # rozkladyApp
 *
 * Main module of the application.
 */
angular
    .module('rozkladyApp', [
        'ngCookies',
        'ngMaterial',
        'ui.router',
        'snap'
    ]).config(function ($stateProvider, $urlRouterProvider) {

        'use strict';

        $stateProvider
            .state('home', {
                url: '/',
                templateUrl: 'views/selectStop.html',
                controller: 'MainCtrl'
            })
            .state('stop', {
                url: "/:slug",
                templateUrl: 'views/selectStop.html',
                controller: 'MainCtrl'
            });
        $urlRouterProvider.otherwise("/");
    });
