'use strict';

/**
 * @ngdoc function
 * @name rozkladyApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the rozkladyApp
 */
angular.module('rozkladyApp')
    .controller('MainCtrl', function ($scope, $http,$rootScope, $stateParams, $cookies, $state, HomeFactory) {
        $rootScope.api = 'http://api.rozklady.bartosz.jakubowiak.pl/api/v1/';


        $scope.filters = {};
        $scope.favStop = null;
        $scope.slug = $stateParams.slug;
        $scope.HomeFactory = HomeFactory;
        $scope.favStop = $cookies.favStop;
        $scope.showMap = false;
        $scope.showSearch = false;
        if ($scope.slug) {
            HomeFactory.listDepartures($scope.slug);
        }
        else {
            HomeFactory.getFavoriteStop();
        }


        $scope.parseLoc = function (data) {
            $scope.$apply(function () {
                $scope.getProximityStops(data);
            });
        };
        navigator.geolocation.getCurrentPosition($scope.parseLoc);

        $scope.getProximityStops = function (data) {

            $http.post($scope.api + 'close-to-me', {
                lat: data.coords.latitude,
                lon: data.coords.longitude
            }).success(function (data) {

                $scope.proximity = data;

            });

        };

        $scope.closeMap = function () {
            $scope.showMap = false;
        };

        $scope.closeSearch = function () {
            $scope.showSearch = false;
        };

        $scope.toggleMap = function () {

            $scope.closeSearch();
            $scope.showMap = !$scope.showMap;
            $scope.loadMap();
        };

        $scope.toggleSearch = function () {
            $scope.closeMap();
            $scope.showSearch = !$scope.showSearch;
        };

        $scope.loadMap = function () {
            angular.extend($scope, {
                map: {
                    center: {
                        longitude: 19.94266,
                        latitude: 50.05941
                    },
                    options: {
                        streetViewControl: false
                    },
                    zoom: 15,
                    events: {
                        tilesloaded: function (map) {
                            $scope.mapLoaded(map);
                        }
                    }

                }
            });


            $scope.mapLoaded = function (map) {
                $scope.$apply(function () {


                    var bounds = map.getBounds();
                    var pos = {
                        lefttop: {
                            x: bounds.na.j,
                            y: bounds.va.j
                        },
                        rightbottom: {
                            x: bounds.na.k,
                            y: bounds.va.k
                        }
                    }
                    $scope.loadMarkers(pos, map.getZoom());
                });
            };


            $scope.onMarkerClicked = function (marker) {
                $state.go('stop', {slug: marker.slug});

            };

            $scope.loadMarkers = function (data, zoom) {

                if (zoom >= 13) {
                    $http.post('/api/v1/get-markers', data)
                        .success(function (data) {

                            var dynamicMarkers = data;

                            _.each(dynamicMarkers, function (marker) {
                                marker.closeClick = function () {
                                    marker.showWindow = false;
                                    $scope.$apply();
                                };
                                marker.onClicked = function () {
                                    $scope.onMarkerClicked(marker);
                                };
                            });
                            $scope.map.dynamicMarkers = dynamicMarkers;

                        });
                } else {
                    $scope.map.dynamicMarkers = [];
                }
            };
        };
    });
