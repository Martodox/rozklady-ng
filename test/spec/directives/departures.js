'use strict';

describe('Directive: departures', function () {

  // load the directive's module
  beforeEach(module('rozkladyApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<departures></departures>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the departures directive');
  }));
});
