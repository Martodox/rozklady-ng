'use strict';

describe('Service: HomeFactory', function () {

  // load the service's module
  beforeEach(module('rozkladyApp'));

  // instantiate service
  var HomeFactory;
  beforeEach(inject(function (_HomeFactory_) {
    HomeFactory = _HomeFactory_;
  }));

  it('should do something', function () {
    expect(!!HomeFactory).toBe(true);
  });

});
